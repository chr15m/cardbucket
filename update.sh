#!/bin/bash

ssh -A cardbucket@cardbucket.net "cd ~/cardbucket && . ./virtualenv/bin/activate && git pull && ./manage.py syncdb && ./manage.py migrate && touch site.wsgi"
