<http://cardbucket.net/>

Agile/scrum cards from BitBucket issues.

Because Bitbucket Cards isn't open.

Hint: use milestones for sprints.

Hack
----

  * `virtualenv virtualenv`
  * `. ./virutalenv/bin/activate`
  * `pip install -r requirements.txt`
  * `git submodule init && git submodule update`
  * `./manage.py runserver`

License
-------

AGPLv3 license.

Copyright Chris McCormick, George Atherley, 2013.

With contributions from Prathamesh Paiyyar.
