import os, sys

sys.path.insert(0, os.path.abspath(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(os.path.abspath(os.path.dirname(__file__)), "virtualenv", "bin"))

import activate_this

os.environ['DJANGO_SETTINGS_MODULE'] = 'cardboardbucket.settings'
os.environ['PYTHON_EGG_CACHE'] = '/var/cache/www/pythoneggs'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
