# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'BitbucketUser'
        db.create_table(u'cardboardbucket_bitbucketuser', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('last_visited_repo', self.gf('django.db.models.fields.CharField')(max_length=300, null=True)),
        ))
        db.send_create_signal(u'cardboardbucket', ['BitbucketUser'])


    def backwards(self, orm):
        # Deleting model 'BitbucketUser'
        db.delete_table(u'cardboardbucket_bitbucketuser')


    models = {
        u'cardboardbucket.bitbucketuser': {
            'Meta': {'object_name': 'BitbucketUser'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_visited_repo': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        }
    }

    complete_apps = ['cardboardbucket']