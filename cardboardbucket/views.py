import httplib2
from urllib import urlencode
import json

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse

import models

from rauth import OAuth1Service
import requests

import settings


REQUEST_TOKEN_URL = 'https://bitbucket.org/!api/1.0/oauth/request_token'
ACCESS_TOKEN_URL = 'https://bitbucket.org/!api/1.0/oauth/access_token'
AUTHORIZE_URL = 'https://bitbucket.org/!api/1.0/oauth/authenticate'

PUT_REQUEST = "/put"
POST_REQUEST = "/post"
DELETE_REQUEST = "/delete"

SUCCESS_CODE = 200


def home(request, accountname=None, repo_slug=None):
	url = "%s%s" % (settings.BB_API_URL, "user")
	
	if request.session.has_key("bitbucket_session"):
		session = request.session.get("bitbucket_session")
		if session:
			resp = session.get(url)
			if resp.status_code == SUCCESS_CODE:
				resp_struct = json.loads(resp.text)
				user, created = models.BitbucketUser.objects.get_or_create(username=resp_struct['user']['username'])
				
				if user:
					if not repo_slug and user.last_visited_repo:
						return HttpResponseRedirect(user.last_visited_repo)

					if repo_slug:
						user.last_visited_repo = "/%s/%s/" % (accountname, repo_slug)
						user.save()

	return render_to_response("index.html", context_instance=RequestContext(request))


def api(request, put, path):
	url = "%s%s" % (settings.BB_API_URL, path)
	#print url
	session = request.session.get("bitbucket_session")
	if session:
		if PUT_REQUEST == put:
			resp = session.put(url, data=request.GET)
		elif POST_REQUEST == put:
			resp = session.post(url, data=request.GET)
		elif DELETE_REQUEST == put:
			resp = session.delete(url, data=request.GET)
		else:
 			resp = session.get(url, params=request.GET)
	else:
		# tell the client side to run oauth authentication first
		class R:
			text = json.dumps("auth")
		resp = R()
	#print resp.text
	return HttpResponse(resp.text and resp.text or "null", mimetype="text/plain")

def check_auth_required(request, repo):
	url = "%s%s" % (settings.BB_API_URL, "repositories/%s/issues" % (repo,))
	s = request.session.get("bitbucket_session", requests.Session())
	r = s.get(url)
	if r.status_code == 200:
		response = "allowed"
		# use this session for future requests if we haven't got access yet
		if not request.session.get("bitbucket_session"):
			request.session["bitbucket_session"] = s
	else:
		response = "denied"
	return HttpResponse(json.dumps(response), mimetype="text/plain")

def get_bitbucket_session(request):
	# Create the service
	bitbucket = OAuth1Service(name='bitbucket',
		consumer_key=settings.BITBUCKET_CONSUMER_KEY,
		consumer_secret=settings.BITBUCKET_CONSUMER_SECRET,
		request_token_url=REQUEST_TOKEN_URL,
		access_token_url=ACCESS_TOKEN_URL,
		authorize_url=AUTHORIZE_URL
	)
	# Make the request for a token, include the callback URL.
	rtoken, rtoken_secret = bitbucket.get_request_token(params={'oauth_callback': 'http://' + request.get_host() + '/got_bitbucket_session/'})
	# store the token and secret for later
	request.session["rtoken"] = rtoken
	request.session["rtoken_secret"] = rtoken_secret
	# Use the token to rquest an authorization URL.
	return HttpResponseRedirect(bitbucket.get_authorize_url(rtoken))

def got_bitbucket_session(request):
	# Create the service
	bitbucket = OAuth1Service(name='bitbucket',
		consumer_key=settings.BITBUCKET_CONSUMER_KEY,
		consumer_secret=settings.BITBUCKET_CONSUMER_SECRET,
		request_token_url=REQUEST_TOKEN_URL,
		access_token_url=ACCESS_TOKEN_URL,
		authorize_url=AUTHORIZE_URL
	)
	request.session["bitbucket_session"] = bitbucket.get_auth_session(request.session["rtoken"], request.session["rtoken_secret"], data={'oauth_verifier': request.GET.get("oauth_verifier")})
	return HttpResponseRedirect("/")

def settings_js(request):
	
	settings_dict = {
		"ROOT_URL": request.build_absolute_uri(reverse("home")),
		"MEDIA_URL": settings.MEDIA_URL,
		"API_URL": reverse('api'),
		"LOGOUT_URL": reverse('auth_logout'),
		"BITBUCKET_AUTHENTICATED": request.session.get("rtoken_secret") and True or False,
	}
	return render_to_response("settings.js", {"settings_json": json.dumps(settings_dict)}, mimetype="application/javascript", context_instance=RequestContext(request))
