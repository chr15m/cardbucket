/**
 * js functionality related on cards.
 */
MILESTONE_LABEL = "milestone";
COMPONENT_LABEL = "component";
VERSION_LABEL = "version";

TAG_NOT_SELECTED = "Not Selected";

STATUS_NEW = "new";
STATUS_OPEN = "open";
STATUS_RESOLVED = "resolved";
STATUS_ON_HOLD = "on hold";
STATUS_INVALID = "invalid";
STATUS_DUPLICATE = "duplicate";
STATUS_WONTFIX = "wontfix";

/**
 * This is card widget. It represents a card.
 * All the functionality and characteristics should be included in this 
 * widget.
 * 
 * Methods:
 * get_tag - returns the tag title which currently under clicked. 
 *           Currently used by during modification of tags.
 * get_milestone - returns current milestone of this card
 * get_component - returns current component of this card
 * get_version - returns current version of this card
 * get_card_id- returns card id
 * load_tags_list(tag, content) - Load tags in the update tag dialog box.
 *     param: tag - tag title.
 *     param: content - dialog box body element.
 * set_selected_tag_indication(tag) - Used by update tag dialog box. 
 *     Selected the tag item from dialog box list which is currently selected.
 * bind_tag_item_events(tag) - Used by update tag dialog box.
 *     Bind events for tag item in the dialog box.
 * change_tag(tag_title, tag_name) - Update cards tag.
 *     param: tag_title - Possible options miletsone, component, version
 *     param: tag_name - name of desired tag, provided that tag should already
 *                       exists.
 * reload - Reload the card
 * 
 * Options:
 * card: Input parameter. JSON recieved from server for a particular.
 * change_tag_dialog_id - ID attr of the html element that represent 
 *                        update tag dialog box.
 * bottom_menu - selector for bottom menu in the card
 * tag - selector for tags in the bottom menu of the card
 * tag_change - selector of tag's element which triggers tag changing operation
 * milestone - selector of milestone tag container
 * component - selector of component tag container
 * version - selector of version tag container
 * progress_indicator - html snippet that represent loading or progress.
 */
$.widget( "custom.cards", {
    // default options
    options: {
        card: null,
        
        change_tag_dialog_id: 'change_tag_dialog',

        close_button: '.button_to_invalid',
        bottom_menu: '.colum_item_bottom_menu',
        tag: '.colum_item_bottom_menu .tag',
        tag_change: '.colum_item_bottom_menu .tag .change',

        milestone: '.colum_item_bottom_menu .tag.milestone',
        component: '.colum_item_bottom_menu .tag.component',
        version: '.colum_item_bottom_menu .tag.version',
        
        progress_indicator: "<div class='fill_middle'><p></p><img src='/media/img/spinner.gif'/></div>"
    },

    /**
     * @constructor
     */
    _create: function() {
        this.element
        // add a class for theming
        .addClass( "custom-card" )
        // prevent double click to select text
        // .disableSelection();
        
        this._on( $(this.element).find(".colum_item_bottom_menu i.fa"), {
            click: "_toggle_comments"
        });

        this._refresh();
    },
 
    /**
     * @private
     * Called when created, and later when changing options
     */
    _refresh: function() {
        // tags
        this.tag_elements = this.element.find(this.options.tag_change);
        // bind click events on change
        this._on( this.tag_elements, {
          // _on won't call random when widget is disabled
          click: "_changeTag"
        });
        
        this.close = this.element.find(this.options.close_button);
        // bind click events on change
        this._on( this.close, {
          // _on won't call random when widget is disabled
          click: "_close"
        });

        // this._populateTags();
        this._initChangeTagDialog();
    },
    
    /**
     * @private
     * @returns tag name based on the tag title.
     */
    _get_tag_name: function(tag_title) {
        return $(this.tag_elements).parents(".tag")
               .filter("." + tag_title).find("span.tag_name").text();
    },
    
    /**
     * @private
     * Triggers a dialog box to select/change a tag
     */
    _changeTag: function( event ) {
        var tag = this.get_tag(event.currentTarget);
        
        $(window.change_tag_dialog)
        .dialog("option","widget_instance",this.element)
        .dialog("option","tag",tag)
        .dialog("option","title",capFirst(tag) + "s")
        .dialog("open");
        event.preventDefault();
    },
    
    /**
     * @private
     * Initialized the dialog to display when changing of tags.
     * Append dialog code to document body.
     */
    _initChangeTagDialog: function() {
        if ( !window.change_tag_dialog ) {
            var el = $("<div><div/>");
            $(el).attr("id",this.options.change_tag_dialog_id);

            $("body").append(el);

            var id  = "#" + this.options.change_tag_dialog_id;
            var dialog = $(id).addClass("custom-change-tag-dialog").dialog({
                autoOpen: false,
                resizable: false,
                height: 300,
                width: 400,
                modal: true,
                open: this._loadTags,
            });

            window.change_tag_dialog = dialog;
        }
        
    },
    
    /**
     * @private
     * load tags list and also bind events to listen for selection.
     */
    _loadTags: function() {
        var card_widget = $(this).dialog("option","widget_instance");
 
        var spinner = $(card_widget).cards("option","progress_indicator");
        $(spinner).find("p").text("Loading.");
        $(this).html($(spinner));

        var tag = $(this).dialog("option","tag");
        var content = $("#" + $(card_widget).cards("option","change_tag_dialog_id"));
        $(card_widget).cards("load_tags_list", tag, content);
    },
    
    /**
     * @private
     * Load comments and toggle display of comments section
     */
    _toggle_comments: function( event ) {
        $(event.currentTarget).toggleClass("fa-arrow-circle-o-up");
        $(event.currentTarget).toggleClass("fa-arrow-circle-o-down");
        
        var comments_item = $(event.currentTarget).parents(".colum_item").find(".colum_item_comments")
        $(comments_item).find(".fill_middle").hide();
        $(comments_item).toggleClass("comments-visible").slideToggle();

        // if ( $(comments_item).hasClass("comments-visible") ) {
            // $(comments_item).find(".fill_middle").show();
            // $(comments_item).find(".no-comments").remove();
            // $(comments_item).find(".comment").remove();
// 
            // this._load_description(comments_item);
            // // this._load_comments(comments_item);
        // }
    },

    /**
     * @private
     */    
    _load_comments: function(comments_item) {
        // load comments
        var url = "/api/repositories/" + get_repo_path() + "/issues/" + this.get_card_id() + "/comments/";
        // console.log(url);
        $.getJSON( url, function(data) {
            // console.log(data);
            var comments = Mustache.render(template['comments.html'], {"comments": data, "formatDate": formatDateFilter, "parseWikiHandler": parse_wiki_handler});
            // console.log(comments);
            $(comments_item).find(".fill_middle").hide();

            if ( comments.trim() ) {
                $(comments_item).append(comments);
            } else {
                $(comments_item).append("<div class='no-comments'>No comments found.</div>");
            }
        }).error(function() {
            alert("Error loading comments. Refresh and try again.");
        });
    },
    
    /**
     * @private
     */    
    _load_description: function(comments_item) {
        // load comments
        var url = "/api/repositories/" + get_repo_path() + "/issues/" + this.get_card_id() + "/";
        // console.log(url);
        $.getJSON( url, function(data) {
            // console.log(data);
            var comments = Mustache.render(template['description.html'], {"issue": data, });
            // console.log(comments);
            $(comments_item).find(".fill_middle").hide();

            if ( comments.trim() ) {
                $(comments_item).append(comments);
            } else {
                $(comments_item).append("<div class='no-comments'>No description found.</div>");
            }
        }).error(function() {
            alert("Error loading comments. Refresh and try again.");
        });
    },

    /*
     * 
     * Marks card as invalid
     */
    _close: function() {
        if ( confirm("Do you want to mark Card " + this.get_card_id() + " as invalid?") ) {
            this.set_status(STATUS_INVALID);
        }
    },
 
    /**
     * gets tag name for the clicked element
     */
    get_tag: function(el) {
        if ( $(el).hasClass("change_milestone") ) {
            return "milestone";
        } else if ( $(el).hasClass("change_component") ) {
            return "component";
        } else if ( $(el).hasClass("change_version") ) {
            return "version";
        } else {
            return "";
        }
    },
    
    /**
     * @return - current milestone of this card
     */
    get_milestone: function() {
        // return this._get_tag_name(MILESTONE_LABEL);
        return this.options.card.metadata.milestone;
    },
    
    /**
     * @return - current component of this card
     */
    get_component: function() {
        // return this._get_tag_name(COMPONENT_LABEL);
        return this.options.card.metadata.component;
    },
    
    /**
     * @return - current version of this card
     */
    get_version: function() {
        // return this._get_tag_name(VERSION_LABEL);
        return this.options.card.metadata.version;
    },
    
    /**
     * @return - card id
     */
    get_card_id: function() {
        // return $(this.element).attr("id").split("_").pop();
        return this.options.card.local_id;
    },
    
    /**
     * Load tags in the update tag dialog box.
     * 
     * @param {Object} tag - tag title
     * @param {Object} content -dialog box body element
     */
    load_tags_list: function(tag, content) {
        var tags_url = cardbucket_settings.API_URL + "/repositories/" + get_repo_path() + "/issues/" + tag + "s";
        var card_widget = this;

        // request the tags list from the server
        $.getJSON(tags_url, function(data) {
            data = sortTags(data);
            data.push({"id": 0, "name": TAG_NOT_SELECTED})
            var str = Mustache.render(template["tags_list.html"], $.extend({}, settings, {"tags":data}));
            $(content).html(str);

            card_widget.set_selected_tag_indication(tag);
            card_widget.bind_tag_item_events(tag);
        }).error(function() {
            $(content).children(".fill_middle").find("p").text("Error Loading List.");
            $(content).children(".fill_middle").find("img").hide();
        });
    },
    
    /**
     * Selected the tag item from dialog box list which is currently selected.
     * 
     * @param {Object} tag - tag title 
     */
    set_selected_tag_indication: function(tag) {
        var currentSelectedTagName = this["get_"+tag]();
        if ( !currentSelectedTagName ) {
            $("#" + this.options.change_tag_dialog_id + " .tag#tag_0").addClass("selected");
        } else {
            $("#" + this.options.change_tag_dialog_id + " .tag .name")
            .each(function() {
            
                if( currentSelectedTagName === $(this).text() ) {
                    // console.log($(this).parent(".tag"));
                    $(this).parent(".tag").addClass("selected");
                }
                
            });
        }
         
    },
    
    /**
     * Bind events for tag item in the dialog box.
     * 
     * @param {Object} tag - tag title
     */
    bind_tag_item_events: function(tag) {
        var instance = this;
        $("#" + this.options.change_tag_dialog_id + " .tag:not(.selected)")
        .on("click", function() {
            if ( !$(this).hasClass("saving") ) {
                $(this).addClass("new_selection");
                $(this).parent().find(".tag").addClass("saving");
                
                var tag_name = $(this).children(".name").text();
                instance.change_tag(tag, tag_name);
            }
        });
    },
    
    /**
     * Update cards tag.
     * 
     * @param {Object} tag_title - Possible options miletsone, component, version 
     * @param {Object} tag_name - name of desired tag, provided that tag should already exists.                       
     */
    change_tag: function(tag_title, tag_name) {
        var instance = this;
        // console.log("change_tag", this.get_card_id());
        var url = cardbucket_settings.API_URL + "/put/repositories/" 
                    + get_repo_path() + "/issues/" + this.get_card_id();
        var data = {};
        if ( tag_name == TAG_NOT_SELECTED ) {
            data[tag_title] = "";
        } else {
            data[tag_title] = tag_name;
        }

        $.ajax({
            url: url,
            data: $.param(data),
            success: function(data, textStatus, jqXHR) {
                instance.reload();
                $(window.change_tag_dialog).dialog("close");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(textStatus);
                $(window.change_tag_dialog).dialog("close"); 
            },
        });
    },
    
    /**
     * Status status of this card
     */
    set_status: function(status) {
        var instance = this;
        //console.log("set_status", this.get_card_id(), status);
        var url = cardbucket_settings.API_URL + "/put/repositories/" 
                    + get_repo_path() + "/issues/" + this.get_card_id();
        var data = {};
        data["status"] = status;

        $.ajax({
            url: url,
            data: $.param(data),
            success: function(data, textStatus, jqXHR) {
                instance.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(textStatus);
            },
        });
    },
    
    /**
     * Reloads the card
     */
    reload: function() {
        $(document).bind("fetched_cards", function() {
            // update the filters based on milestone preset filter, which takes precedence
            apply_milestone_preset();
            // filter out the ones the user doesn't want to see
            filter_visible_cards();
            // unbind event
            $(document).unbind("fetched_cards");
        });
        
        fetch_cards();
    },
    
    /**
     * Update timestamp of this card
     */
    update_timestamp: function(callback) {
        //console.log("update_timestamp", this.get_card_id());
        var url = cardbucket_settings.API_URL + "/put/repositories/" 
                    + get_repo_path() + "/issues/" + this.get_card_id();
        var errCode = 0;
        var cardEl = this.element;

        // send request to update timestamp
        $.ajax({
            url: url,
            beforeSend: function(jqXHR, settings) {
                $(cardEl).find(".spinner").show(); // show spinner
            },
            success: function(data, textStatus, jqXHR) {
                errCode = 0;
                // console.log(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                errCode = 1;
            },
            complete: function(jqXHR, textStatus) {
                $(cardEl).find(".spinner").hide(); // hide spinner
                callback(null, errCode);
            }
        })

        return errCode;
    },

    /**
     * @private
     * events bound via _on are removed automatically
     * revert other modifications here
     */
    _destroy: function() {
        // clean-up
    },

    /**
     * @private
     * _setOptions is called with a hash of all options that are changing
     * always refresh when changing options
     */
    _setOptions: function() {
        // _super and _superApply handle keeping the right this-context
        this._superApply( arguments );
        this._refresh();
    },

    /**
     * @private
     * _setOption is called for each individual option that is changing
     */
    _setOption: function( key, value ) {
        if ( key == "card" ) {
            return;
        }

        this._super( key, value );
    },

});
