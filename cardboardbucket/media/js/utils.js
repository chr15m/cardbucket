/**
 * 
 */

function getCardID(el) {
    return $(el).parents(".colum_item").attr("id").split("_").pop();
}

function bind_task_events() {
    $("a.assign_task").on('click', function() {
        var cardId = getCardID($(this));
        show_user_list(null, cardId);

        return false;
    });
    
    $("a.reassign_task").on('click', function() {
        var cardId = getCardID($(this));
        var user = $("#card_" + cardId).find('.colum_item_date input:hidden.colum_item_user').val();
        
        show_user_list(user, cardId);

        return false;
    });
};

function show_user_list(user, cardId) {

    var buttonLabel = "Assign";
    if ( user != null ) {
        buttonLabel = "Reassign";
    }
    $("#task_allocation_dialog").remove();
    var dialog = $(Mustache.render(template["task_allocation_dialog.html"], settings))
                 .appendTo("body")
                 .dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 300,
                    width: 400,
                    modal: true,
                    title: "Select User",
                    open: function() {
                        load_users_list(user, dialog);
                    }
                 });

    var assign_task = function() {
        var newUser = $("#selectable-user").find("li.ui-selected").attr("data-user");
        
        if ( newUser == user ) {
            // user not changed.
            $(dialog).dialog("close");
            return;
        }
        
        // console.log("assign_task", cardId);
        $.ajax({
            url: cardbucket_settings.API_URL + "/put/repositories/" + get_repo_path() + "/issues/" + cardId,
            data: $.param({ "responsible": $("#selectable-user").find("li.ui-selected").attr("data-user") }),
            beforeSend: function(jqXHR, settings) {
                $(dialog).find(".fill_middle").show()
                .find("p").text("Saving.");
                $(dialog).find("ul").hide();
            },
            success: function(data, textStatus, jqXHR) {
                reload_card(cardId);
                $(dialog).dialog("close");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $(dialog).find(".fill_middle p").text(textStatus);
                $(dialog).find(".fill_middle img").hide(); 
            },
            async: false
        });
    };
    var buttons = [
        { text: buttonLabel, click: assign_task },
        { text: 'Cancel', click: function() { $(this).dialog("close"); } }
    ];
    $(dialog).dialog("option","buttons",buttons);
    $(dialog).dialog("open");
}

function get_userlist( data ) {
    var userlist = [];
    for( i=0; i < data.length; i++ ) {
        if (i == 0 && data[i]["repository"] && data[i]["repository"]["owner"] && !data[i]["repository"]["owner"]["is_team"]) {
            userlist.push(data[i]["repository"]["owner"]);
        }
        if (data[i]["user"]) {
            userlist.push(data[i].user);
        }
        if (data[i]["group"] && data[i]["group"]["members"]) {
            for (var m=0; m<data[i]["group"]["members"].length; m++) {
                userlist.push(data[i].group.members[m]);
            }
        }
    }
    return userlist;
}

function load_users_list(user, dialog) {
    // request the user list from the server
    async.parallel({
        repo_privilege_users: function(callback){
            var url = cardbucket_settings.API_URL + "/privileges/" 
                        + get_repo_path() + "/?" + $.param({"filter":"write"});
            $.getJSON(url, function(data) {
                if ( data ) {
                    callback(null, get_userlist(data));
                }
            }).error(function() {
                callback(null, []);
            });
        },
        group_privilege_users: function(callback){
            var url = cardbucket_settings.API_URL + "/group-privileges/" 
                        + get_repo_path() + "/?" + $.param({"filter":"write"});
            $.getJSON(url, function(data) {
                if ( data ) {
                    callback(null, get_userlist(data));
                }
            }).error(function() {
                callback(null, []);
            });
        }
    },
    function(err, results) {
        // console.log(results);
        var userlist = [];
        var dup_check = {};
        // build a user list from the API calls
        for (var u in results) {
            for ( i=0; i<results[u].length; i++ ) {
                var new_user = results[u][i];
                // only add the new user if they are not present already
                if (!dup_check[new_user.username]) {
                    dup_check[new_user.username] = true;
                    userlist.push(new_user);
                    // check for responsible user
                    if (user == new_user.username) {
                        new_user["is_responsible"]= true;
                    }
                }
            }
        }
        // empty user to un-assign
        userlist.push({"display_name": "No User"});
        var ul = $(Mustache.render(template["user_list.html"], {"userlist": userlist}));
        ul.selectable();
        $(dialog).append(ul);
        $(dialog).find(".fill_middle").hide();
    });

    
}

function reload_card( cardId ) {
    fetch_cards();
}

function bind_tags_events() {
    $(".filter_category .manage_tags").on("click", function(ev) {
        var tag = $(this).attr("id").split("_").pop();
        
        // Dialog Box Clean-up.
        var dialogEl = $("#manage_dialog");
        $(dialogEl).find(".form_container").hide()
        .find(".new_form").removeClass("saving").find("input:text").val("");
        $(dialogEl).find(".dialog-title-bar a.create-new").show();

        $(dialogEl).find(".tags_list").empty().hide();
        $(dialogEl).find(".form_title").empty();
        $(dialogEl).find(".new_form input:text").val("");
        
        var dialog = initManageTagDialog();
        $(dialog).dialog("option","title","Manage " + capFirst(tag));
        $(dialog).dialog("option","tag",tag);

        // $("#manage_dialog").children(".fill_middle").show().find("p").text("Loading " + capFirst(tag));
        $(dialog).dialog("open");
        ev.preventDefault();
    });
}

function initManageTagDialog() {
    var dialog = $("#manage_dialog").dialog({
        autoOpen: false,
        resizable: false,
        height: 400,
        width: 500,
        modal: true,
        open: function() {
            // Load Tags List.
            loadTagsList($(this).dialog("option","tag"));
            
            // Bind Create New Funcationality
        }
    });
    
    return dialog;
}

function loadTagsList(tag) {
    var tags_url = cardbucket_settings.API_URL + "/repositories/" + get_repo_path() + "/issues/" + tag + "s";
    var dialogEl = $("#manage_dialog");

    $(dialogEl).children(".tags_list").hide();
    $(dialogEl).children(".fill_middle").show().find("p").text("Loading " + capFirst(tag));

    // request the tags list from the server
    $.getJSON(tags_url, function(data) {
        data = sortTags(data);
        // console.log(data);
        $(dialogEl).children(".fill_middle").hide();

        var str = Mustache.render(template["tags_list.html"], $.extend({}, settings, {"tags":data}));
        $(dialogEl).find(".tags_list").html(str).show('slow');
        
        // Bind Edit Funcationality
        bind_edit_tags_events();
    }).error(function() {
        $(dialogEl).children(".fill_middle").find("p").text("Error Loading " + capFirst(tag) + "s List.");
    });
}

function bind_edit_tags_events() {
    $("#manage_dialog .tags_list .tag .name").on("click", function() {
        var tag = $(this).parents(".tag");

        if ( !$(tag).hasClass("deleting") ) {
            $(tag).find(".edit input[name=tag]").val($(this).text());
            $(tag).addClass("editing");
        }
    });

    $("#manage_dialog .tags_list .tag .edit input:button").each(function() {
        $(this).button();
    });

    $("#manage_dialog .tags_list .tag .edit input[name=cancel]").on("click", function() {
        $(this).parents(".tag").removeClass("editing");
    });
    
    $("#manage_dialog .tags_list .tag .edit input[name=save]").on("click", function() {
        var tagID = $(this).parent(".edit").siblings("div.name").attr("id");
        var tagName = $(this).siblings("input[name=tag]").val().trim();

        saveTag(tagID,tagName);
    });

    $("#manage_dialog .tags_list .tag a.remove").on("click", function() {
        var tagID = $(this).siblings("div.name").attr("id");
        deleteTag(tagID);

        return false;
    });
}

function manage_tag_dialog() {
    
    $("#manage_dialog .dialog-title-bar a.create-new").on("click", function() {
        $(this).hide();
        
        var cont = $("#manage_dialog .form_container");
        $(cont).find(".new_form").removeClass("saving").find("input:text").val("");
        $(cont).slideDown('slow');
        
        return false;
    });
    
    $("#manage_dialog .form_container .new_form input[name=cancel]").on("click", function() {
        $("#manage_dialog .dialog-title-bar a.create-new").show();
        $("#manage_dialog .form_container").slideUp();
    });
    
    $("#manage_dialog .form_container .new_form input[name=add]").on("click", function() {
        var tagName = $(this).siblings("input[name=new_tag]").val().trim();
        
        if ( tagName ) {
            // $("#manage_dialog .dialog-title-bar a.create-new").show();
            createTag(tagName);            
        }
    });
    
    $("#manage_dialog input:button").each(function() {
        $(this).button();
    });
}

function createTag(tagName) {
    var dialogEl = $("#manage_dialog");
    var tag = $(dialogEl).dialog("option","tag");
    var createTagUrl = cardbucket_settings.API_URL + "/post/repositories/" + get_repo_path() + "/issues/" + tag + "s";

    $.ajax({
        url: createTagUrl,
        data: $.param({ "name": tagName }),
        beforeSend: function(jqXHR, settings) {
            $(dialogEl).find(".form_container .new_form").addClass("saving");
            // console.log("reached");
        },
        success: function(data, textStatus, jqXHR) {
            loadTagsList(tag);
            $(dialogEl).find(".form_container").hide()
            .find(".new_form").removeClass("saving").find("input:text").val("");
            $(dialogEl).find(".dialog-title-bar a.create-new").show();
            
            refreshTag(tag);
            // $(dialogEl).dialog("close");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(textStatus + ". Closing Dialog Box.");
            $(dialogEl).dialog("close");
        },
    });
}

function saveTag(tagID, tagName) {
    
    if ( !tagName ) {
        return;
    }

    var dialogEl = $("#manage_dialog");
    var tag = $(dialogEl).dialog("option","tag");
    // console.log("saveTag", tag, tagID);
    var saveTagUrl = cardbucket_settings.API_URL + "/put/repositories/" + get_repo_path() + "/issues/" + tag + "s/" + tagID;

//    console.log(tagID);
//    $(dialogEl).find("#tag_" + tagID).removeClass("editing").addClass("saving")
//    .find(".edit input:text").attr("disabled","disabled");
    
    $.ajax({
        url: saveTagUrl,
        data: $.param({ "name": tagName }),
        beforeSend: function(jqXHR, settings) {
            $(dialogEl).find("#tag_" + tagID).addClass("saving")
            .find(".edit input:text").attr("disabled","disabled");
        },
        success: function(data, textStatus, jqXHR) {
            loadTagsList(tag);
            refreshTag(tag);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(textStatus + ". Closing Dialog Box.");
            $(dialogEl).dialog("close");
        },
    });

}

function deleteTag(tagID) {
    var dialogEl = $("#manage_dialog");
    var tag = $(dialogEl).dialog("option","tag");
    var deleteTagUrl = cardbucket_settings.API_URL + "/delete/repositories/" + get_repo_path() + "/issues/" + tag + "s/" + tagID;

    $.ajax({
        url: deleteTagUrl,
        beforeSend: function(jqXHR, settings) {
            $(dialogEl).find("#tag_" + tagID).addClass("deleting");
        },
        success: function(data, textStatus, jqXHR) {
            loadTagsList(tag);
            refreshTag(tag);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(textStatus + ". Closing Dialog Box.");
            $(dialogEl).dialog("close");
        },
    });
}

function refreshTag(tag) {
    var tags_url = cardbucket_settings.API_URL + "/repositories/" + get_repo_path() + "/issues/" + tag + "s";

    // request the issues list from the server
    $.getJSON(tags_url, function(data) {

        var results = {"category":tag, "tags":data};
        for (var t=0; t<results.tags.length; t++) {
            results.tags[t].check = filters[results.category] && filters[results.category].indexOf(results.tags[t].name) != -1;
        }
        // add the content column these issues will go into
        $("#filters").find("#filter_"+tag).replaceWith(Mustache.render(template["filter.html"], $.extend({}, settings, results)));

        // binding events for tags
        bind_tags_events();
    }).error(function() {
        alert("Could reload " + tag + "s");
    });
}

function capFirst(str) {
    var words = str.split(" ");
    for(var i=0; i < words.length; i++) {
        words[i] = words[i].charAt(0).toUpperCase() + words[i].substr(1); 
    }
    
    return words.join(" ")
}
