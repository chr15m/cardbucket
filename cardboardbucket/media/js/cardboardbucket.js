// HTML fragments stored in a global
var template = {};
var template_files = ["login.html", "signout.html", "projects.html", 
                      "loading.html", "column.html", "card.html", "filter.html", 
                      "project_link.html", "add_card_link.html", "user_list.html",
                      "task_allocation_dialog.html", "tags_list.html", 
                      "comments.html", "description.html", "filter_users.html"];

// tags - component, milestone, version
var tag_categories = ["milestone", "component", "version", "users"];

// statuses and ways they are ordered
var statuses = [
	[1, "new", "todo"],
	[2, "open", "doing"],
	[3, "on hold", "QA"],
	[4, "resolved", "done"]
];

// global that will store the current filter parameters
var filters = {};
var milestone_preset_filter = null;
var current_milestone = null;

// global store of all card data we know about
var cards = {};

// settings to pass into all templates
var settings = {"settings": window.cardbucket_settings};

function get_repo_path() {
    var repo_slug = $.url().attr("path").split("/").slice(1,3).join("/");
    if ( !repo_slug.split("/").pop() ) {
        return;
    }
    return repo_slug;
}

function get_user() {
    return get_repo_path().split("/").reverse().pop();
}

function get_repo() {
    return get_repo_path().split("/").pop();
}

$(function() {
	// check URL for milestone filter sets
	// all = every card, backlog = only cards with no milestone, current/blank = auto-filter to latest milestone
	milestone_preset_filter = $.url().attr('fragment');
	
	// check URL for existing filter criteria and apply it to checkboxes and cards
	for (var tc=0; tc<tag_categories.length; tc++) {
		var category = tag_categories[tc];
		filters[category] = $.url().param(category) ? $.url().param(category) : [];
	}
	
	// update the filters based on milestone preset filter, which takes precedence
	apply_milestone_preset();
	
	// load the template files we will use to render things
	for (t = 0; t < template_files.length; t++) {
		$.get("/media/templates/" + template_files[t], function(tn, t) {
			return function(data) {
				template[tn] = data;
				template_files.pop();
				// once we've loaded each template
				if (template_files.length == 0) {
					$(document).trigger('templates_loaded');
				}
			}
		}(template_files[t], t));
	}
	
	// once templates are loaded test for auth
	$(document).on("templates_loaded", function() {
		var repository = get_repo_path();

		// see if we are looking at a repo
		if (repository) {
			// append the project link to the top left menu
			$("#menu_top_logo").append(Mustache.render(template["project_link.html"], $.extend({}, settings, {"repository": repository})));
			// check if the current user can view this repo
			$.getJSON("/check_auth_required/" + repository, function(data) {
				$("#content").html("");
				if (data == "allowed") {
					// show the signout UI
					show_signout();
					// show them the cards
					fetch_cards();
					fetch_tags();
					// show the "add card" link
					$("#action_buttons").prepend(Mustache.render(template["add_card_link.html"], $.extend({}, settings, {"repository": repository})));
				} else {
					// show them a login UI
					show_login(true);
				}
			});
		} else {
			// we should show a list of the user's projects
			// see if we are signed in yet
			$.getJSON(cardbucket_settings.API_URL + "/user", function(data) {
				$("#content").html("");
				if (data == "auth" || !data) {
					// show the login link (point to bitbucket)
					show_login();
				} else {
					// show the signout UI
					show_signout();
					// fetch the project list
					fetch_projects();
				}
			});
		}
	});

	// once all tags and cards have been loaded
	$(document).when("fetched_tags fetched_cards").done(function() {
		// update the filters based on milestone preset filter, which takes precedence
		apply_milestone_preset();
		// filter out the ones the user doesn't want to see
		filter_visible_cards();
		
	    fetch_users();
	});
	
	// when filters are rolled over
	$(document).on("mouseover", "a.filter_category", function(ev) {
		$(this).find(".filter_options").show();
		$(this).find("i.fa-chevron-circle-right").removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
	});
	$(document).on("mouseout", "a.filter_category", function(ev) {
		$(this).find(".filter_options").hide();
		$(this).find("i.fa-chevron-circle-down").removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
	});
	$(document).on("click", "a.filter_category,.filter_options", function(ev) {
		// don't want to do anything with a click on the top level element
		if (ev.target == this) {
			ev.preventDefault();
		}
	});
	
	// when filter labels are selected, click the filter
	$(document).on("click", ".filter_options label", function(ev) {
		$(this).parent().find("input").trigger("click");
		ev.preventDefault();
	});
	
	// when a filter is changed
	$(document).on("change", ".filter_options input", function(ev) {
		var selected = [];
		//console.log($(this).parent().parent().attr("id"));
		var category = $(this).parent().parent().attr("id").split("_").pop();
		$(this).parent().parent().find("input:checked").each(function(i, el) {
			selected.push($(el).val());
		});
		// the milestone preset filter should be adjusted if they have selected milestone filters
		if (category == "milestone") {
			// if no selection, or only the top selection is chosen
			if (selected.length && !(selected.length == 1 && selected[0] == current_milestone)) {
				milestone_preset_filter = "all";
			} else {
				milestone_preset_filter = null;
			}			
		}
		// store selected filters in our global
		filters[category] = selected;
		// make the selected filters compatible with the milestone presets
		apply_milestone_preset()
		// update the URL based on the filter settings
		rebuild_url();
		// apply filter to existing cards
		filter_visible_cards();
	});
	
	// when one of the milestone preset filters is clicked
	$(document).on("click", ".milestone_preset_filter", function(ev) {
		milestone_preset_filter = $(this).html();
		// update the filters based on milestone preset filter
		apply_milestone_preset();
		// update the URL based on the filter settings
		rebuild_url();
		// perform the actual filtering of the cards
		filter_visible_cards();
		ev.preventDefault();
	});
	
	manage_tag_dialog();
	
	// as jQuery to defeat the cache
	$.ajaxSetup({"cache": false });
});

function filter_visible_cards() {
	$(".colum_item").each(function(i, el) {
		var cid = parseInt($(this).attr("id").split("_").pop());
		var show_it = true;
		for (var tc=0; tc<tag_categories.length; tc++) {
			var category = tag_categories[tc];
			if ( category == "users" ) {
			    if ( filters[category].length && !(cards[cid].responsible && cards[cid].responsible['username'] && filters[category].indexOf(cards[cid].responsible["username"]) != -1) ) {
                    show_it = false;
                }
			} else {
    			if (filters[category].length && !(cards[cid].metadata[category] && filters[category].indexOf(cards[cid].metadata[category]) != -1)) {
    				show_it = false;
    			}
			}

			// test for backlog / all milestone preset filter
			if (category == "milestone") {
				// backlog filter - hide cards with milestone tags
				if (milestone_preset_filter == "backlog" && cards[cid].metadata[category] && cards[cid].metadata[category].length) {
					show_it = false;
				}
				// current filter - hide cards that don't match the latest milestone
				if (!milestone_preset_filter && cards[cid].metadata[category] != current_milestone) {
					show_it = false;
				}
			}
		}
		show_it ? $(el).show(): $(el).hide();
		$(el).cards({'card':cards[cid]});
	});	
}

// build and update the current URL the user sees based on filter settings
function rebuild_url() {
	var url = $.url().attr("protocol") + "://" + $.url().attr("host") + ($.url().attr("port") != "80" ? ":" + $.url().attr("port") : "") + $.url().attr("path") + ($.param(filters) ? "?" + $.param(filters) : "") + (milestone_preset_filter ? "#" + milestone_preset_filter : "");
	history.pushState(null, null, url);
}

// applys the milestone pre-set filter to the milestones filter
function apply_milestone_preset() {
	// the "current" filter is represented as a blank string
	if (milestone_preset_filter == "current" || milestone_preset_filter == "") {
		milestone_preset_filter = null;
	}
	// the blank filter defaults to only the latest milestone
	if (!milestone_preset_filter || milestone_preset_filter == "backlog") {
		// reset filters to null
		filters["milestone"] = [];
		// unselect all milestone filters
		$("#filter_milestone input").each(function(i, e) {
			if (!milestone_preset_filter && !i) {
				$(e).attr("checked", true);
				// console.log('checked', e);
			} else {
				$(e).attr("checked", false);
			}
		});
	}
	// now make sure the current_milestone is indicated in the menu
	$(".milestone_preset_filter").removeClass("active");
	$("#milestone_preset_" + (milestone_preset_filter || "current")).addClass("active");
}

function make_columns_sortable() {
		$(".column").sortable({
			"connectWith": ".column",
			"over": function(ev, ui) {
				// console.log("over", ev, ui);
				// $(ui.sender).css("background-color", "#32708d");
			}, "out": function(ev, ui) {
				// $(ui.sender).css("background-color", null);
			}, "change": function(ev, ui) {
				// console.log("change", ev, ui);
				//var cid = $(ui.item).attr("id").split("_").pop();
				//console.log("CHANGE", cid);
			}, "update": function(ev, ui) {
				// console.log("update", ev, ui);
				//console.log("UPDATE", ui.sender, ui.item, this);
				//console.log("HOW MANY", $(this).has($(ui.item)).length);
				// if we are the column being dropped on and there was no sender (sort only)
				if ($(this).has($(ui.item)).length && !ui.sender) {
					//console.log("update_timestamp");
					$(".column").sortable("disable");
					var items = $(ui.item).prevAll(".colum_item").toArray().reverse();
					items.push($(ui.item));
					async.filterSeries(items.reverse(), function(el, callback) {
						// console.log(el);
						$(el).cards("update_timestamp", callback);
					}, function(err, results) {
						$(".column").sortable("enable");
					});
				}
			}, "receive": function(ev, ui) {
				//console.log("RECEIVE");
				$(".column").sortable("disable");
				
				var new_status = statuses[parseInt($(this).attr("id").split("_").pop()) - 1][1];
				var cid = $(ui.item).attr("id").split("_").pop();
				//console.log("receive", cards[cid]);
				//console.log("make_columns_sortable receive", cid, new_status);
				
				// TODO: use card.set_status() instead
				var issue_update_url = cardbucket_settings.API_URL + "/put/repositories/" + get_repo_path() + "/issues/" +  cards[cid].local_id + "?status=" + new_status;
				// console.log(issue_update_url);
				$(ui.item).find(".spinner").show();
				$.getJSON(issue_update_url, function(data) {
					// console.log("updated", data);
					$(ui.item).find(".spinner").hide();
					$(".column").sortable("enable");
				}).error(function() {
					$(".column").sortable("enable");
				});
			}
		}).disableSelection();
}

function loading(message) {
	$("#content").html(Mustache.render(template["loading.html"], $.extend({}, settings, {"message": message})));
}

function show_login(repo_specified) {
	var login_button = $(Mustache.render(template["login.html"], $.extend({}, settings, {"repo_specified": repo_specified})));
	login_button.click(function(ev) {
		loading("Signing in.");
	});
	$("#content").html(login_button);
}

function show_signout() {
	$("#action_buttons").html(Mustache.render(template["signout.html"], settings));
}

function fetch_projects() {
	loading("Fetching your projects.");
	$.getJSON(cardbucket_settings.API_URL + "/user/repositories/dashboard", function(data) {
		// console.log(data);
		var projects = [];
		for (var p=0; p<data.length; p++) {
			projects.push({
				"username": data[p][0].username,
				"repositories": data[p][1]
			});
		}
		$("#content").html(Mustache.render(template["projects.html"], $.extend({}, settings, {"projects": projects, "make_link": function() {
			return function(data, render) {
				var link = decodeURIComponent(render(data));
				return link.substring(1, link.length - 1);
			}
		}})));
	});
}

function fetch_tags() {
	// launch an ajax request for every tag category
	async.map(tag_categories.slice(0,-1), function(item, callback) {
		// console.log(item);
		var tags_url = cardbucket_settings.API_URL + "/repositories/" + get_repo_path() + "/issues/" + item + "s";
		// console.log(issues_url);
		// request the issues list from the server
		$.getJSON(tags_url, function(data) {
			// console.log(data);
			callback(null, {
				"tags": data,
				"category": item
			});
		}).error(callback);
	}, function(err, results){
		// once we get all category tags back
		console.log("Tags loaded", err, results);
		for (var r=0; r<results.length; r++) {
			var sortedTags = sortTags(results[r].tags);
			// remember the current/latest milestone
			if (results[r].category == "milestone") {
				current_milestone = sortedTags[0].name;
			}
			results[r].tags = sortedTags;
			for (var t=0; t<results[r].tags.length; t++) {
				results[r].tags[t].check = filters[results[r].category] && filters[results[r].category].indexOf(results[r].tags[t].name) != -1;
			}
			// add the content column these issues will go into
			$("#filters").append(Mustache.render(template["filter.html"], $.extend({}, settings, results[r])));
		}
		// binding events for tags
		bind_tags_events();
		// throw an event to say we have all tags
		$(document).trigger('fetched_tags');
	});
}

function fetch_cards() {
	loading("Fetching project cards.");
	// launch an ajax request for every status and do an async.parallel for them
	async.map(statuses, function(item, callback) {
		// console.log(item);
		var issues_url = cardbucket_settings.API_URL + "/repositories/" + get_repo_path() + "/issues?" + $.param({"limit": 50, "status": item[1], "sort": "-utc_last_updated"});
		// console.log(issues_url);
		// request the issues list from the server
		$.getJSON(issues_url, function(data) {
			data.source = {
				"num": item[0],
				"status": item[1],
				"title": item[2]
			};
			callback(null, data);
		}).error(callback);
	}, function(err, results) {
		// once we get all issues back
		$("#content #loader").remove();
		console.log("Issues loaded:", err, results);
		for (var r=0; r<results.length; r++) {
			// add the content column these issues will go into
			$("#content").append(Mustache.render(template["column.html"], $.extend({}, settings, results[r].source)));
			for (var c=0; c<results[r].issues.length; c++) {
				var card = results[r].issues[c];
				var card_html = $(Mustache.render(template["card.html"], $.extend({}, settings, card, {"repository": get_repo_path(), "formatDate": formatDateFilter}))).hide();
				$("#column_" + results[r].source.num).append(card_html);
				cards[card.local_id] = card;
			}
		}
		// make the cards sortable
		make_columns_sortable();
		// Bind card events
		bind_task_events();
		// throw an event to say we have all cards
		$(document).trigger('fetched_cards');
	});
}

function fetch_users() {
    // loading("Fetching project users.");
    async.parallel({
        repo_privilege_users: function(callback){
            var url = cardbucket_settings.API_URL + "/privileges/" 
                        + get_repo_path() + "/?" + $.param({"filter":"write"});
            $.getJSON(url, function(data) {
                if ( data ) {
                    callback(null, get_userlist(data));
                }
            }).error(function() {
                callback(null, []);
            });
        },
        group_privilege_users: function(callback){
            var url = cardbucket_settings.API_URL + "/group-privileges/" 
                        + get_repo_path() + "/?" + $.param({"filter":"write"});
            $.getJSON(url, function(data) {
                if ( data ) {
                    callback(null, get_userlist(data));
                }
            }).error(function() {
                callback(null, []);
            });
        },
        get_responsible_users: function(callback) {
            var users = [];
            for( cardId in cards ) {
                var card = cards[cardId];
                if ( card['responsible'] ) {
                    users.push(card['responsible']);
                }
            }

            callback(null, users);
        }
    },
    function(err, results) {
        // console.log(results);
        var userlist = [];
        var dup_check = {};
        // build a user list from the API calls
        for (var u in results) {
            for ( i=0; i<results[u].length; i++ ) {
                var new_user = results[u][i];
                // only add the new user if they are not present already
                if (!dup_check[new_user.username]) {
                    dup_check[new_user.username] = true;
                    userlist.push(new_user);
                }
            }
        }

        console.log("Users loaded:", err, results);
        $("#filters").append(Mustache.render(template["filter_users.html"], {"users":userlist, "repo_slug":get_repo_path()}));
        bind_userlist_events();

        $(document).trigger('fetched_users');
    });

}

function bind_userlist_events() {
    $("#filters a#filter_users p#invite_new_user").on({
        click: function(event) {
            window.open("https://bitbucket.org/" + get_repo_path(), "_blank");
        }
    });
}

function sortTags(tags) {
    return tags.sort(function(tagA, tagB) { return tagB["id"] - tagA["id"]; });
}

function formatDateFilter() {
	return function(text, render) {
		return (new Date(render(text))).toFormat("YYYY-MM-DD HH24:MI");
	}
}
