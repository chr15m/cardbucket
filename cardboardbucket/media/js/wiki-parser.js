
// Formatting
var patterns = [
    // TODO [/\`{3}((\s)*(\n)*(.*)(\s)*(\n)*\`{3}/, "<code>$1</code>"], // Code: 
    
    [/\*\*(.*?)\*\*/g, "<b>$1</b>"], // Bold,
    [/\*(.*?)\*/g, "<i>$1</i>"], // Italic
    
    [/#{3}\s(.*)#{3}/, "<h3>$1</h3>"], // Heading 3
    [/#{2}\s(.*)#{2}/, "<h2>$1</h2>"], // Heading 2
    [/#\s(.*)#/, "<h1>$1</h1>"], // Heading 1
    
    [/\!\[(.*?)\][\(](.*?)[\)]/, "<img src=\"$2\" title=\"$1\"/>"], // Image
    [/\[(.*?)\][\(](.*?)[\)]/, "<a href=\"$2\">$1</a>"], // Anchor/URL
];

var unordered_list_depth = 0;
var ordered_list_depth = 0;


function parse_wiki_handler() {
    return function(text, render) {
        return parse_wiki(render(text));
    };
}

function parse_wiki(input_data)
{
    if ( !input_data ) {
        return;
    }

    var output = "";
    var lines = input_data.split("\n");

    for (var i=0; i<lines.length; i++)
    {
        for ( var j=0; j<patterns.length; j++ ) {
            lines[i] = lines[i].replace(patterns[j][0], patterns[j][1]);
        }

        lines[i] = replace_lists(lines[i]);
        output += lines[i] + "\n";
    }
    
    return output;    
}

function replace_lists(data)
{
    var output = "";
    var unordered_pattern = /^\*\s(.*)$/;
    // var ordered_pattern = /^\#\s(.*)$/;
    var ordered_pattern = /^\d+[.]\s(.*)$/;
    
    if (unordered_list_depth > 0 && data.match(unordered_pattern) == null)
    {
        unordered_list_depth = unordered_list_depth - 1;
        output += "</ul>";        
    }
    
    if (ordered_list_depth > 0 && data.match(ordered_pattern) == null)
    {
        ordered_list_depth = ordered_list_depth - 1;
        output += "</ol>";        
    } 
    
    if (unordered_list_depth == 0 && data.match(unordered_pattern) != null)
    {
        output += "<ul>";
        unordered_list_depth += 1;
    }
    
    if (unordered_list_depth > 0 && data.match(unordered_pattern) != null)
    {
        output = output + data.replace(unordered_pattern, "<li>$1</li>");
    } 
    
    if (ordered_list_depth == 0 && data.match(ordered_pattern) != null)
    {
        output += "<ol>";
        ordered_list_depth += 1;
    }
    
    if (ordered_list_depth > 0 && data.match(ordered_pattern) != null)
    {
        output = output + data.replace(ordered_pattern, "<li>$1</li>");
    } 
    
    if (output == "</ul>")
    {
        output += data;
    }
    
    if (output == "</ol>")
    {
        output += data;
    }
    
    if (output.length == 0)
    {
        output += data;
    }
    
    return output;
}
