from django.conf.urls import patterns, include, url
from django.contrib import admin

import settings

admin.autodiscover()

urlpatterns = patterns('',
	# Examples:
	url(r'^projects/$', 'django.shortcuts.render', {"template_name": "index.html"}, name='projects'),
	url(r'^$', 'cardboardbucket.views.home', name='home'),
	url(r'^(?P<accountname>[A-Za-z0-9_-]+)/(?P<repo_slug>[A-Za-z0-9_-]+)/$', 'cardboardbucket.views.home', name='home'),
	
	url(r'^api(?P<put>(/put|/post|/delete){0,1})/(?P<path>.*)$', 'cardboardbucket.views.api', name="api"),
	url(r'^api$', 'cardboardbucket.views.api', name="api"),
	url(r'^logout/$', 'django.contrib.auth.views.logout', {"next_page": "/"}, name='auth_logout'),
	url(r'^get_bitbucket_session/$', 'cardboardbucket.views.get_bitbucket_session', name="get_bitbucket_session"),
	url(r'^got_bitbucket_session/$', 'cardboardbucket.views.got_bitbucket_session', name="got_bitbucket_session"),
	url(r'^check_auth_required/(?P<repo>.*)$', 'cardboardbucket.views.check_auth_required', name="check_auth_required"),
	url(r'^settings.js$', 'cardboardbucket.views.settings_js', name='settings_js'),
	# url(r'^cardboardbucket/', include('cardboardbucket.foo.urls')),
	
	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
	
	# Uncomment the next line to enable the admin:
	url(r'^admin/', include(admin.site.urls)),
)

# serve media files statically in debug mode
if settings.DEBUG or hasattr(settings, 'SELF_SERVE'):
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
	)

